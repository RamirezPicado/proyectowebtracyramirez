<?php
require_once '../shared/guard.php';
$title = 'Crear Questionnaires';
require_once '../partials/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $long_description = filter_input(INPUT_POST, 'long_description', FILTER_SANITIZE_STRING);
    $questionnaires_model->insert($description, $long_description);
    return header('Location: /questionnaires');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <form method="POST">
    <div class="form-group">
      <label for="description">Description</label>
      <input type="text" class="form-control" placeholder="description" name="description">
    </div>
    <div class="form-group">
      <label for="long_description">long_description</label>
      <input type="text" class="form-control" placeholder="long_description" name="long_description">
    </div>
    <input class="btn btn-primary" type="submit" value="Aceptar">
    <a class="btn btn-default btn-danger" href="/questionnaires">Cancelar</a>
  </form>
</div>
