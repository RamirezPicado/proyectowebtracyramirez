<?php include "../config.php"; ?>
<?php include './partials/header.php' ?>
  <?php include './partials/navbar.php' ?>
    <div class="container">
      <!-- Content here -->
      <div class="row">
         <div class="col-md-8">
           <?php if ($_SESSION['access_denied'] ?? null) : ?>
             <div class="alert alert-warning alert-dismissible fade show" role="alert">
               <strong>¡Error!</strong> <?= $_SESSION['access_denied']; ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <?php $_SESSION['access_denied'] = null; ?>
           <?php endif; ?>
         </div>
     </div>
      <div class="row">
        <div class="col-sm-6">
          <h1>Login</h1>
          <form action="/actions/authenticate.php" method="post">
            <div class="form-group">
              <label for="username">Usuario:</label>
              <input
                type="text" class="form-control"
                id="username" name="username"
                aria-describedby="Intruduzca su nombre de usuario"
                placeholder="">
            </div>
            <div class="form-group">
              <label for="password">contraseña:</label>
              <input type="password" class="form-control"
                id="password" name="password"
                placeholder="">
            </div>
            <button type="submit" class="btn btn-primary">Iniciar sesión</button>
          </form>
        </div>
      </div>
    </div>
    <?php include './partials/footer.php' ?>
