<?php
namespace models{
    class question{

        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM  WHERE id = $1', [$id])[0];
        }
        
       public function select(){
      
        return $this->connection->runQuery('SELECT * FROM questions ORDER BY id');
    
       }

       public function insert($questionnaire_id, $question_text)
       {
           $this->connection->runStatement('INSERT INTO questions(questionnaire_id,question_text) VALUES ($1, $2)', [$questionnaire_id,$question_text]);
       }
    }
} 
   
    
     

    