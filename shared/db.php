<?php
 require_once __DIR__ . '/../Db/PgConnection.php';


 use Db\PgConnection;

 require_once __DIR__ . '/../models/questions.php';


 return [
/*$con = new PgConnection('postgres', '12345', 'isw613_questionnaires', 5432, 'localhost');
$con->connect();



    /*
    |--------------------------------------------------------------------------
    | Parámetros generales
    |--------------------------------------------------------------------------
    | Definir en los parámetros generales de la aplicación
    */
   'debug'      => true,
    'public'     => "/",

    /*
    |--------------------------------------------------------------------------
    | Database parameters
    |--------------------------------------------------------------------------
    | En esta sección se deben definir los parámetros de conexión a BD
    */
    'server'       => 'localhost',
    'database'     => 'isw613_questionnaires',
    'user'         => 'isw613_user',
    'password'     => 'secret',
    'driver_class' => 'MyApp\Database\SqlMySql',
];
